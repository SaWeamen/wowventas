﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WowVentas.Entidades
{
    public class ItemVenta
    {
        public ItemVenta(int cantidad, Producto producto)
        {
            Cantidad = cantidad;
            Producto = producto;
        }
        public int Cantidad { get; set; }
        public Producto Producto { get; set; }
        public override string ToString()
        {
            return string.Format("{0} ${1} x {2}=${3}", Producto.Nombre, Producto.Costo, Cantidad, Producto.Costo * Cantidad);
        }
    }
}
