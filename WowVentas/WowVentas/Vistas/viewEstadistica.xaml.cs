﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowVentas.Entidades;
using WowVentas.Modelos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowVentas.Vistas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class viewEstadistica : ContentPage
	{
        Repositorio<Producto> repositorioProductos;
        Repositorio<Venta> repositorioVentas;
		public viewEstadistica ()
		{
			InitializeComponent ();
            repositorioVentas = new Repositorio<Venta>();
            repositorioProductos = new Repositorio<Producto>();
            List<Estadistica> datos = new List<Estadistica>();
            foreach (var producto in repositorioProductos.Read)
            {
                datos.Add(CalcularVenta(producto.Nombre));
            }

            PlotModel model = new PlotModel();
            LinearAxis ejeX = new LinearAxis();
            ejeX.Position = AxisPosition.Bottom;

            CategoryAxis ejeY = new CategoryAxis();
            ejeY.Position = AxisPosition.Left;

            model.Axes.Add(ejeY);
            model.Axes.Add(ejeX);

            model.Title = "Ventas";
            ColumnSeries columas = new ColumnSeries();
            columas.Title = "Monto";
            ColumnItem item;
            foreach (var dato in datos)
            {
                item = new ColumnItem();
                item.
                item.Value = dato.Monto;
                columas.Items.Add(item);
            }
            model.Series.Add(columas);
            chrEstadistica.Model = model;
		}

        private Estadistica CalcularVenta(string nombre)
        {
            int cantidad = 0;
            float total = 0;
            foreach (var venta in repositorioVentas.Read)
            {
                foreach (var producto in venta.Items)
                {
                    if (producto.Producto.Nombre == nombre)
                    {
                        cantidad += producto.Cantidad;
                        total += producto.Cantidad * producto.Producto.Costo;
                    }
                }
            }
            return new Estadistica()
            {
                Producto = nombre,
                Cantidad = cantidad,
                Monto = total
            };
        }
    }
}