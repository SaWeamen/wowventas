﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WowVentas.Modelos
{
    public class Estadistica
    {
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public float Monto { get; set; }
    }
}
